module Calculator

open Fileparser

let transform (fileRecs: FileRecord seq) =
    let flist = Seq.toList fileRecs
    let inOut = List.partition (fun fileRec -> fileRec.recordType = In) flist
    let earliestEnter = List.minBy (fun item -> item.time) (fst inOut)
    let latestExit = List.maxBy (fun item -> item.time) (snd inOut)
    latestExit.time - earliestEnter.time


let graph sym (limit: int) (max: int) current =
    let coeff = (float limit) / (float max)
    let count = int <| System.Math.Round((float current) * coeff)
    String.replicate count sym


let printElement max (element: System.DateTime * int) =
    let totalMins = (snd element)
    let hours = totalMins / 60
    let mins = totalMins - (hours * 60)
    let dateTime = fst element
    let dateText = dateTime.Day.ToString() + 
                    "." + dateTime.Month.ToString()
    let graphstr = graph "-" 20 max totalMins
    printfn "%10s %10s  %2d:%2d    %s" 
        dateText (dateTime.DayOfWeek.ToString()) 
        hours 
        mins 
        graphstr


let printTable (days: (System.DateTime * int) seq) =
    let max = snd <| Seq.maxBy (fun el -> (snd el)) days
    Seq.iter (printElement max) days
    days


let byMonth day =
    let dtime: System.DateTime = fst day
    dtime.Month


let dayDiff day =
    let dayTime = snd day
    dayTime - 5 * 60


let calculateForMonth month =
    let monthID = fst month
    let days = snd month
    let diff = Seq.sumBy dayDiff days
    (monthID, diff)


let printMonthStat (month, totalMins) =
    let hours = totalMins / 60
    let mins = totalMins - (hours * 60)
    printfn "Month: %d  overtime: %d:%d" month hours mins

