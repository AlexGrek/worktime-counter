module Fileparser

type InOrOut =
    | In
    | Out

type FileRecord = {
        date: System.DateTime; 
        time: int; 
        recordType: InOrOut;
        day: string
     }

let parseDateTime (str: string) =
    let arr = str.Split([|'.'|])
    let month = (int arr.[1])
    let day = (int arr.[0])
    let year = (int arr.[2])
    let date = System.DateTime(year, month, day)
    date

let parseTimeToMins (str: string) =
    let arr = str.Split([|'.'|])
    let hours = int arr.[0]
    let minutes = int arr.[1]
    hours * 60 + minutes


let parseType = function
| "in"  -> In
| "out" -> Out
| x     -> failwith ("Unknown in/out type: " + x)


let parse (str: string) =
    let arr = str.Split([|','|])
    {   date = (parseDateTime arr.[0]); 
        time = (parseTimeToMins arr.[2]);
        recordType = (parseType arr.[1]);
        day = arr.[3]
    }


let parseFile filename = 
     System.IO.File.ReadLines(filename)
     |> Seq.map parse
