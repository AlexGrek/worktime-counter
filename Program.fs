﻿open System
open Fileparser
open Calculator

let groupByDates (recs: FileRecord seq) = 
    Seq.groupBy (fun x -> x.date ) recs



[<EntryPoint>]
let main argv = 
    parseFile "test.txt"
    |> groupByDates
    |> Seq.map (fun x -> ((fst x),(transform (snd x))))
    |> printTable
    |> Seq.groupBy byMonth
    |> Seq.map calculateForMonth
    |> Seq.iter printMonthStat
    printfn "Hello World!"
    0 // return an integer exit code
